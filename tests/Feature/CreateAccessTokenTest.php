<?php

namespace Tests\Feature;

use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateAccessTokenTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create Access Token.
     *
     * @return void
     */
    public function test_create_token()
    {
        $this->seed(UserSeeder::class);

        $response = $this->postJson('v1/tokens', [
            'email' => 'admin@example.com',
            'password' => 'password',
            'tokenName' => 'Test Token',
        ]);

        $response->assertStatus(201)
            ->assertJson(
                fn (AssertableJson $json) => $json->has('data')
            );
    }
}
