<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateUserCommandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_create_command()
    {
        $this->artisan('user:create "John Doe" "john@example.com" "password"')
            ->assertExitCode(0);
        $this->artisan('user:create "John Doe" "john@example.com" "password"')
            ->assertExitCode(2);
    }
}
