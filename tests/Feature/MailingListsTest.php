<?php

namespace Tests\Feature;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MailingListsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Mailig lists API tets.
     *
     * @return void
     */
    public function test_get_mailing_lists_api()
    {
        $this->seed(DatabaseSeeder::class);

        $response = $this->getJson('v1/lists');

        $response->assertJsonPath('paginate.total', 4);
    }
}
