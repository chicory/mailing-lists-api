<?php

namespace Tests\Feature;

use App\Models\MailingList;
use App\Models\User;
use Database\Seeders\TestDataSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscriptionsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Get subsriber's subscriptions list test.
     *
     * @return void
     */
    public function test_get_subscriber_subscriptions_api()
    {
        $this->prepare();

        $response = $this->getJson('v1/subscriptions/1');

        $response->assertJsonStructure([
            'paginate' => [
                'offset',
                'limit',
                'total',
            ]
        ])->assertOk();
    }

    /**
     * Get mailing list subsribers.
     *
     * @return void
     */
    public function test_get_mailing_list_subscribers_api()
    {
        $this->prepare();

        $response = $this->getJson('v1/subscribers/' . $this->getListId());

        $response->assertOk();
    }

    /**
     * Subscripton test.
     *
     * @return void
     */
    public function test_subscribe_api()
    {
        $this->prepare();

        $response = $this->postJson('v1/subscribers/' . $this->getListId(), [
            'email' => 'janedoe@example.com'
        ]);

        $response->assertCreated();
    }

    /**
     * Unubscribe test.
     *
     * @return void
     */
    public function test_unsubscribe_api()
    {
        $this->prepare();

        $response = $this->deleteJson('v1/subscribers/' . $this->getListId(), [
            'email' => 'first@example.com'
        ]);

        $response->assertNoContent();
    }

    /**
     * Unubscribe all test.
     *
     * @return void
     */
    public function test_unsubscribe_all_api()
    {
        $this->prepare();

        $response = $this->deleteJson('v1/subscribers', [
            'email' => 'first@example.com'
        ]);

        $response->assertNoContent();
    }


    private function getListId()
    {
        return MailingList::where('name', 'Health')->get()->first()->id;
    }

    private function prepare()
    {
        return $this->seed()->seed(TestDataSeeder::class)
            ->actingAs(User::where('email', 'admin@example.com')->first());
    }
}
