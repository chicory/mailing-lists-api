# Laravel Mailing Lists
Laravel test application.

## System requirements
* PHP 8+
* PHP BCMath
* PHP Ctype
* PHP Fileinfo
* PHP JSON
* PHP Mbstring
* PHP OpenSSL
* PHP PDO
* PHP Tokenizer
* PHP XML
* PHP GDLib
* composer

## Installation
```bash
git clone https://codeberg.org/chicory/mailing-lists-api.git
cd mailing-lists-api
cp .env.example .env

```
Configure DB connection in `.env` file.
```bash
php artisan key:generate
php artisan migrate 
php artisan db:seed
```

## Run
```bash
php artisan serve
```

## API Documentation
**Generate:**
```
php artisan scribe:generate
```
Documentation will be available at `yourdomain.com/docs`

## CLI commands
**Create user:**
```
php artisan user:create {name} {email} {password}
```

## Test data seeding
```
php artisan db:seed --class=TestDataSeeder
```
> *Test user: admin@example.com;password*

## Testing
```
php artisan test
```
