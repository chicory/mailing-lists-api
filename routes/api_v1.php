<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Access tokens manipulation endpoints.
 */
Route::controller(\App\Http\Controllers\v1\AccessTokenController::class)->group(function () {
    Route::post('tokens', 'create');
});

/**
 * Mailing lists endpoints.
 */
Route::controller(\App\Http\Controllers\v1\MailingListController::class)->group(function () {
    Route::get('lists', 'index');
});

/**
 * Subscriptions endpoints.
 */
Route::controller(\App\Http\Controllers\v1\SubscriptionController::class)->group(function () {
    Route::get('subscriptions/{subscriber}', 'showSubscriprions');
    Route::get('subscribers/{list}', 'showSubscribers');
    Route::post('subscribers/{list}', 'subscribe');
    Route::delete('subscribers/{list}', 'unsubscribe');
    Route::delete('subscribers', 'unsubscribeAll');
});
