<?php

namespace Database\Seeders;

use App\Models\MailingList;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MailingListSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MailingList::factory()->createMany([
            ['name' => 'Health'],
            ['name' => 'Fashion'],
            ['name' => 'Education'],
            ['name' => 'Sport'],
        ]);
    }
}
