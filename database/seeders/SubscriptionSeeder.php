<?php

namespace Database\Seeders;

use App\Models\MailingList;
use App\Models\Subscriber;
use App\Models\Subscription;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::factory()
            ->count(5)
            ->state(new Sequence(fn () => [
                'mailing_list_id' => MailingList::all()->random(),
                'subscriber_id' => Subscriber::all()->random(),
            ]))->create();
    }
}
