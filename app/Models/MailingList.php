<?php

namespace App\Models;

use App\Models\Traits\Paginable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{
    use HasFactory;
    use Paginable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'laravel_through_key'
    ];

    /**
     * Get all of the Subscribers for the MailingList.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function subscribers(): HasManyThrough
    {
        return $this->hasManyThrough(
            Subscriber::class,
            Subscription::class,
            'mailing_list_id',
            'id',
            'id',
            'subscriber_id',
        );
    }
}
