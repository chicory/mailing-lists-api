<?php

namespace App\Models\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

/**
 * Offset pagination.
 *
 * Adds offsetPagination(Request $request) method to model.
 *
 * @method \Illuminate\Database\Eloquent\Builder scopeOffsetPaginate(\Illuminate\Database\Eloquent\Builder $query, \Illuminate\Http\Request $request)
 */
trait Paginable
{
    /**
     * Offset pagination.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOffsetPaginate(Builder $query, Request $request): Builder
    {
        return $query->offset($request->offset ?? 0)
            ->limit($request->limit ?? 100);
    }
}
