<?php

namespace App\Models;

use App\Models\Traits\Paginable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;
    use Paginable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'laravel_through_key'
    ];

    /**
     * Get all of the MailingLists for the Subscriber.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function lists(): HasManyThrough
    {
        return $this->hasManyThrough(
            MailingList::class,
            Subscription::class,
            'subscriber_id',
            'id',
            'id',
            'mailing_list_id',
        );
    }
}
