<?php

namespace App\Http\Controllers\v1;

use App\Models\MailingList;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\PaginationRequest;

/**
 * @group Mailing lists
 *
 * Mailing lists endpoints.
 */
class MailingListController extends Controller
{
    /**
     * Mailing lists index
     *
     * @responseFile 200 storage/responses/MailingListsIndexResponse.json
     *
     * @param \App\Http\Requests\v1\PaginationRequest $request
     * @param \App\Models\MailingLis $mailingList
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function index(PaginationRequest $request, MailingList $mailingList): Response|JsonResponse
    {
        return response()
            ->preferredFormat([
                'data' => $mailingList->offsetPaginate($request)
                    ->get(['id', 'name'])
                    ->toArray(),
                'paginate' => [
                    'offset' => (int) $request->offset ?? 0,
                    'limit' => (int) $request->limit,
                    'total' => $mailingList->count(),
                ]
            ])
            ->setStatusCode(Response::HTTP_OK);
    }
}
