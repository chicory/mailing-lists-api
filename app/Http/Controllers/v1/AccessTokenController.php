<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\CreateAccessTokenRequest;

/**
 * @group Aceess Tokens
 *
 * Access tokens manipulation endpoints.
 */
class AccessTokenController extends Controller
{
    /**
     * Create new access token
     *
     * @responseFile 201 storage/responses/NewAccessTokenResponse.json
     *
     * @param \App\Http\Requests\v1\CreateAccessTokenRequest $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function create(CreateAccessTokenRequest $request): Response|JsonResponse
    {
        $request->authenticate();

        return response()
            ->preferredFormat([
                'data' => [
                    'token' => $request->user()
                        ->createToken($request->tokenName)
                        ->plainTextToken
                ]
            ])
            ->setStatusCode(Response::HTTP_CREATED);
    }
}
