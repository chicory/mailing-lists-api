<?php

namespace App\Http\Controllers\v1;

use App\Models\Subscriber;
use App\Models\MailingList;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\PaginationRequest;
use App\Http\Requests\v1\SubscriptionRequest;
use App\Models\Subscription;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

/**
 * @group Subscriptions
 *
 * Subscriptions manipulating endpoints.
 */
class SubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Show subsriber's subscriptions list
     *
     * @authenticated
     * @responseFile 200 storage/responses/MailingListsIndexResponse.json
     *
     * @param \App\Http\Requests\v1\PaginationRequest $request
     * @param \App\Models\Subscriber $subscriber
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function showSubscriprions(PaginationRequest $request, Subscriber $subscriber): Response|JsonResponse
    {
        $subscriptions = $subscriber->lists();
        return response()
            ->preferredFormat([
                'data' => $subscriptions->offsetPaginate($request)
                    ->get(['mailing_lists.id', 'mailing_lists.name'])
                    ->toArray(),
                'paginate' => [
                    'offset' => (int) $request->offset ?? 0,
                    'limit' => (int) $request->limit ?? 100,
                    'total' => $subscriptions->count(),
                ]
            ])->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Show subsribers by mailing list
     *
     * @authenticated
     * @responseFile 200 storage/responses/SubscribersListResponse.json
     *
     * @param \App\Http\Requests\v1\PaginationRequest $request
     * @param \App\Models\MailingList $list
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function showSubscribers(PaginationRequest $request, MailingList $list): Response|JsonResponse
    {
        $subscribers = $list->subscribers();
        return response()
            ->preferredFormat([
                'data' => $subscribers->offsetPaginate($request)
                    ->get(['subscribers.id', 'subscribers.email'])
                    ->toArray(),
                'paginate' => [
                    'offset' => (int) $request->offset ?? 0,
                    'limit' => (int) $request->limit ?? 100,
                    'total' => $subscribers->count(),
                ]
            ])->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Subscribe
     *
     * @authenticated
     * @response 201
     *
     * @param \App\Http\Requests\v1\SubscriptionRequest $request
     * @param \App\Models\MailingList $list
     * @return \Illuminate\Http\Response
     */
    public function subscribe(SubscriptionRequest $request, MailingList $list): Response
    {
        $subscriber = Subscriber::firstOrCreate(['email' => $request->email]);
        Subscription::firstOrCreate([
            'mailing_list_id' => $list->id,
            'subscriber_id' => $subscriber->id,
        ]);
        return response(null, Response::HTTP_CREATED);
    }

    /**
     * Unsubscribe
     *
     * @authenticated
     * @response 204
     *
     * @param \App\Http\Requests\v1\SubscriptionRequest $request
     * @param \App\Models\MailingList $list
     * @return \Illuminate\Http\Response
     */
    public function unsubscribe(SubscriptionRequest $request, MailingList $list): Response
    {
        $subscriber = Subscriber::where(['email' => $request->email])->first();
        if (!$subscriber) {
            abort(404, 'Subscriber not found');
        }
        Subscription::where([
            'mailing_list_id' => $list->id,
            'subscriber_id' => $subscriber->id,
        ])->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Unsubscribe all
     *
     * @authenticated
     * @response 204
     *
     * @param \App\Http\Requests\v1\SubscriptionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function unsubscribeAll(SubscriptionRequest $request): Response
    {
        $subscriber = Subscriber::where(['email' => $request->email])->first();
        if (!$subscriber) {
            abort(404, 'Subscriber not found');
        }
        Subscription::where('subscriber_id', $subscriber->id)->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
