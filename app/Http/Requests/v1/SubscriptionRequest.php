<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'min:8', 'max:128'],
        ];
    }

    /**
     * Parameters descriptions for Scribe.
     *
     * @return array<string, mixed>
     */
    public function queryParameters(): array
    {
        return [
            'email' => [
                'description' => 'Subscriber\'s E-mail address.',
                'example' => 'johndoe@example.com'
            ],
        ];
    }
}
